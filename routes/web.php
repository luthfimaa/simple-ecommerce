<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
    | 
    | Accessable by anyone
    |
*/ 

Route::Auth();
Route::get('/logout', function(){
    Auth::logout();
    redirect('/');
});

Route::get('/'                      , 'HomeController@index')->name('home');
Route::get('/search/'               , 'ProductController@search')->name('search');
Route::get('/product/{id}/detail'   , 'ProductController@detail');
Route::get('/profile/{username}'    , 'ProfileController@show_profile');
Route::get('/category/{category}'   , 'HomeController@show_by_category');
Route::get('/admin'                 , 'HomeController@admin');
Route::get('/pria'                  , 'HomeController@pria');
Route::get('/wanita'                , 'HomeController@wanita');
Route::get('/product/{id}/diskusi'  , 'ProductController@diskusi');



/*
    | 
    | Accessable by member
    |
*/ 

Route::group(['middleware' => ['auth']], function(){

    Route::get('/create/'      , 'ProductController@show');
    Route::get('/profile/'     , 'ProfileController@show_self');
    //Route::get('/profile/{id}' , 'ProfileController@show_profile');
    Route::get('/message'      , 'MessageController@show')->name('message_box');
    Route::get('/products/'    , 'ProductController@show_products');
    Route::get('/edit_profile' , 'ProfileController@edit_form')->name('edit_profile');


    Route::post('/edit_product/'  , 'ProductController@edit_product')->name('edit_product');
    Route::post('/update_product' , 'ProductController@update_product')->name('update_product');
    Route::post('/update_profile' , 'ProfileController@update_profile')->name('update_profile');
    Route::post('/create/'        , 'ProductController@create')->name('create');
    Route::post('/order'          , 'OrderController@order')->name('order');
    Route::post('/message'        , 'MessageController@send')->name('send_message');
    Route::post('/comment'        , 'CommentController@send')->name('send_comment');

});


/*
    | 
    | Accessable by admin
    |
*/ 

Route::group(['middleware' => ['admin']], function(){

    Route::get('/user_list'       , 'AdministrationController@show_users');
    Route::get('/product_list'    , 'AdministrationController@show_products');
    Route::post('/delete_user'    , 'AdministrationController@delete_user')->name('delete_user');
    Route::post('/delete_product' , 'AdministrationController@delete_product')->name('delete_product');
    Route::post('/verify'         , 'AdministrationController@verify_product')->name('verify');
});
?>

