﻿@extends('layouts.app')
@section('content')

        <div id="main">
            <div class="container">
                <div class="row">
                    <!-- About Me (Left Sidebar) Start -->
                    <div class="col-md-3">
                        <div class="about-fixed">

                            <div class="my-pic">
                                <img src={{ asset("storage/photos/$user->photo")}} alt="" style="max-width :192px;">
                            </div>

                            <div class="my-detail">

                                <div class="white-spacing">
                                    <h1>{{ $user->name }} </h1>
                                    
                                        <form action="/message" method="get">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value={{$user->id}}>
                                            <button type="submit" class="btn btn-info">Kirim Pesan</button>
                                        </form>
                                </div> 

                            </div>
                        </div>
                    </div>
                    <!-- About Me (Left Sidebar) End -->
                    <!-- Portfolio (Right Sidebar) Start -->
                    <div class="col-md-9">
                        <div class="col-md-12 page-body">
                            <div class="row">


                                <div class="sub-title">
                                    <h2>Profil Penjual</h2>
                                    <div class="col-sm-6 custom-pad-2">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td><b>Nama</b></td>
                                                        <td>{{ $user->name }}
                                                    </tr>
                                                    <tr>
                                                        <td><b>Nomer hp</b></td>
                                                        <td>{{ $user->phone_number }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            



                        </div>




                    </div>
                    <!-- Portfolio (Right Sidebar) End -->

                </div>
            </div>
        </div>


@endsection
