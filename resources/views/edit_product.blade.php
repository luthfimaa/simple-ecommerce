@extends('layouts.app')
@section('content')

<table>
{{ Form::model($product, ['route' => 'update_product']) }}
{{ Form::hidden('id') }}
    <tbody>
        <tr>
            <td>Nama</td>
            <td>
                {{ Form::text('name') }}
            </td>
        </tr>
        <tr>
            <td>Foto</td>
            <td>
                {{ Form::file('photo') }}
            </td>
        </tr>
        <tr>
            <td>Harga</td>
            <td>
                {{ Form::number('price') }}
            </td>
        </tr>
        <tr>
            <td>Kategori</td>
            <td>
                {{ Form::select('category', ['celana' => 'Celana', 
                                             'sandal' => 'Sandal', 
                                             'kaos' => 'Kaos',
                                             'kemeja' => 'Kemeja',
                                             'sepatu' => 'Sepatu',
                                             'topi' => 'Topi',]) }}
            </td>
        </tr>
        <tr>
            <td>Deskripsi</td>
            <td>
                {{ Form::text('description') }}
            </td>
        </tr>
    </tbody>
</table>
Sold out
{{ Form::hidden('sold', 0) }}
{{ Form::checkbox('sold', 1, $product->sold) }}
Aktifkan komentar
{{ Form::hidden('commentable', 0) }}
{{ Form::checkbox('commentable', 1, $product->commentable) }}
<br>
{{ Form::submit('update') }}
{{ Form::close() }}
@endsection
