
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>login</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="styletika.css">
    <link rel="stylesheet" href="assets/css/Google-Style-Login.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>


  <body>

  <!-- navbar -->
    <ul class="navbar" >
    <li class="nama">parallelogram</li>
  </ul>
  <!-- akhir navbar -->
<div></div>
    <div class="login-card"><img src="assets/img/avatar_2x.png" class="profile-img-card">
        <p class="profile-name-card"> </p>
        <form role="form" method="POST" action="{{ route('login') }}" class="form-signin"><span class="reauth-email"> </span>
    {{ csrf_field() }}
            <input name ="email" class="form-control" type="email" required="" placeholder="Email address" autofocus="" id="inputEmail">
            <input name="password" class="form-control" type="password" required="" placeholder="Password" id="inputPassword">
            <div class="checkbox">
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Remember me</label>
                </div>
            </div>
            <button class="btn btn-primary btn-block btn-sm btn-signin" type="submit">Sign in</button>
        </form>
        <label>Belum punya akun?.</label>
        <a href="register" class="daftar">klik disini</a>
        </div>

  

<!-- footer -->
  <footer >
  <hr>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
      <span id="loncat">
        <p class="text-center" font-family="Goudy stout">© copyright 2017 | design in gazebo FILKOM</p>
      </div>
    </div>

<div class="row">
  <div class="col-sm-12 text-center">
  <a href="https://www.instagram.com/">
  <img src="img/1488064739_instagram_social_media_logo_pin.png " width="50px">
  <i>instagram        </i>
  </a>
  
  <a href="https://www.facebook.com/">
  <img src="img/1488064745_facebook_social_media_logo_pin.png " width="50px">
  <i>facebook         </i>
  </a>
  </div>
</div>
</footer>
<!-- akhir footer -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
<
