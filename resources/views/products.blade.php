
<html>
    <body>
        <h1>
            Daftar product
        </h1>
        <table border="1">
            <thead>
                <th>
                    id
                </th>
                <th>
                    name
                </th>
                <th>
                   price
                </th>
                <th>
                    category
                </th>
                <th>
                    owner id
                </th>
                <th>
                    description
                </th>
                <th>
                    sold out
                </th>
                <th>
                    commentable
                </th>
                <th>
                    verified
                </th>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>
                        {{ $product->id }}
                    </td>
                    <td>
                        <a href={{ "product/$product->id" }}>{{ $product->name }}</a>
                    </td>
                    <td>
                        {{ $product->price }}
                    </td>
                    <td>
                        {{ $product->category }}
                    </td>
                    <td>
                        {{ $product->owner_id }}
                    </td>
                    <td>
                        {{ $product->description }}
                    </td>
                    <td>
                        {{ $product->sold }}
                    </td>
                    <td>
                        {{ $product->commentable }}
                    </td>
                    <td>
                        {{ $product->verified }}
                    </td>
                    <td>
                        {{ Form::open(['route' => 'delete_product']) }}
                        {{ Form::hidden('id', $product->id) }}
                        {{ Form::submit('delete') }}
                        {{ Form::close() }}
                    </td>
                    <td>
                        {{ Form::open(['route' => 'edit_product']) }}
                        {{ Form::hidden('id', $product->id) }}
                        {{ Form::submit('edit') }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        
    </body>
</html>
