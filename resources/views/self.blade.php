@extends('layouts.app')
@section('content')

        <div id="main">
            <div class="container">
                <div class="row">
                    <!-- About Me (Left Sidebar) Start -->
                    <div class="col-md-3">
                        <div class="about-fixed">

                            <div class="my-pic">
                                <img src={{ asset("storage/photos/$user->photo")}} alt="" style="max-width :192px;">
                            </div>

                            <div class="my-detail">

                                <div class="white-spacing">
                                    <h1>{{ $user->name }} </h1>
                                    
                                    <a href="/edit_profile">
                                        <button type="button" class="btn btn-info">Edit Profil</button>
                                    </a>
                                </div> 

                            </div>
                        </div>
                    </div>
                    <!-- About Me (Left Sidebar) End -->
                    <!-- Portfolio (Right Sidebar) Start -->
                    <div class="col-md-9">
                        <div class="col-md-12 page-body">
                            <div class="row">


                                <div class="sub-title">
                                    <h2>Profil Penjual</h2>
                                    <div class="col-sm-6 custom-pad-2">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td><b>Nama</b></td>
                                                        <td>{{ $user->name }}
                                                    </tr>
                                                    <tr>
                                                        <td><b>Nomer hp</b></td>
                                                        <td>{{ $user->phone_number }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            
                                <div class="row">
                                    @if(count($messages))
                                        <h2>Pesan</h2>
                                        <div class="col-sm-6 custom-pad-2">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>From</th>
                                                        <th>Message</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        @foreach($messages as $message)
                                                    <tr>
                                                        <td>{{$message->name}}</td>
                                                        <td>{{$message->message}}</td>
                                                    </tr>
                                        @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-12 content-page">
                                    <div class="col-md-12 blog-post">


                                        <!-- Intro Start -->
                                        <div class="post-title margin-bottom-30">
                                            <h1><span class="main-color">Iklan</span></h1>
                                        </div>
                                        <!-- Intro End -->




                                        <!-- Portfolio Start -->
                                        <div>


                                            <!-- Portfolio Detail Start -->
                                        @foreach($products as $product)
                                            <div class="row portfolio">
                                                <div class="col-sm-6 custom-pad-1">
                                                    <img src={{ asset("storage/photos//$product->photo")}} class="img-responsive" alt="">
                                                </div>


                                                <div class="col-sm-6 custom-pad-2">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <tbody>

                                                                <tr>
                                                                    <td><b>Judul</b></td>
                                                                    <td>{{ $product->name }}</td>
                                                                </tr>


                                                                <tr>
                                                                    <td><b>Harga</b></td>
                                                                    <td>{{ $product->price }}</td>
                                                                </tr>

                                                                <tr>
                                                                    <td><b>Status</b></td>
                                                                    <td>
                                                                    @if($product->sold)
                                                                    {{ "Sold out" }}
                                                                    @else
                                                                    {{ "Tersedia" }}
                                                                    @endif
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td><b>Bisa dikomentari</b></td>
                                                                    <td>
                                                                    @if($product->commentable)
                                                                    {{ "Bisa" }}
                                                                    @else
                                                                    {{ "Tidak" }}
                                                                    @endif
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td><b>Keterangan</b></td>
                                                                    <td>{{ $product->description }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                                <form action="/edit_product" method="POST">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="id" value="{{ $product->id }}">
                                                                    <button type="submit" class="btn btn-info">Edit Barang</button>
                                                                </input>
                                                                </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Portfolio Detail End -->
                                    @endforeach


                                    </div>  


                                </div>

                            </div>



                            <!-- Subscribe Form Start -->
                            <div class="col-md-8 col-md-offset-2">


                            </div>
                            <!-- Subscribe Form End -->


                        </div>




                    </div>
                    <!-- Portfolio (Right Sidebar) End -->

                </div>
            </div>
        </div>


@endsection
