<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pasang Iklan</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/styletika.css">
    <link rel="stylesheet" href="/assets/css/Google-Style-Login.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>


  <body>

  <!-- navbar -->
    <ul class="navbar" >
    <li class="nama">parallerogram</li>
  </ul>
@section('content')
@show

<footer >
<div class="container">
<div class="row">
  <div class="col-sm-12">
  <span id="loncat">
    <p class="text-center" font-family="Goudy stout">© copyright 2017 | design in gazebo FILKOM</p>
  </div>
</div>

<div class="row">
<div class="col-sm-12 text-center">
<a href="https://www.instagram.com/">
<img src="img/1488064739_instagram_social_media_logo_pin.png " width="50px">
<i>instagram        </i>
</a>

<a href="https://www.facebook.com/">
<img src="img/1488064745_facebook_social_media_logo_pin.png " width="50px">
<i>facebook         </i>
</a>
</div>
</div>
</footer>
<!-- akhir footer -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery-3.2.1.min.js"></script>
</body>
</html>
