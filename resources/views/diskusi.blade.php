@extends('layouts.product')

@section('content')
    <section>
<body>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="text-center">Detail Produk</h3> <hr>
          </div>
        </div>

      <div class="row">
      <div class="col-sm-3 col-sm-offset-2">
      <a href="" class="thumbnail">
        <img src='{{ asset("/storage/photos/{$product->photo}") }}'>
        </a>
      </div>
        
        <div class="col-sm-5 ">
          <a href="/product/{{ $product->id }}/detail">
            <button type="button" class="btn btn-info">Informasi produk</button>
          </a>
          <a href="/product/{{ $product->id }}/diskusi">
            <button type="button" class="btn btn-info">Tanya penjual</button><br> <br>
          </a>
<!--
          <div class="col-sm-7">
            <h4>Deskripsi Produk</h4> <hr>
          </div>
      <div class="col-sm-6">
          <table class="table">
            {{ $product->description }}
          </table>
--!>
      </div>


        <div class="col-sm-5 col-sm-offset-1 text-center">
          <h3 class="te">Informasi penjual</h3>
            <img src={{ asset("storage/photos/$owner->photo") }} class="profile-img-card" style="border-radius: 50%; max-width:192px; max-height:192px;">
            <form action="/order" method="post" class="form-signin"><span class="reauth-email"> </span>
            {{ csrf_field() }}
            <p>Nama : {{ $owner->name }}
            <button class="btn btn-primary btn-block btn-sm btn-signin" type="submit">Beli</button>
            {{ Form::hidden('product_id', $product->id) }}
            {{ Form::hidden('buyer_id', $user_id) }}
        </form>
        </div>
        </div>
        </div>

        <section>
        <div class="row">
        <div class="col-lg-6 col-sm-offset-2">
        <a name="chat" ></a>
            <h4 class="">Ada pertanyaan mengenai produk ini?</h4>
          </div>
          <div class="col-sm-6 col-sm-offset-2">
            <form action="/comment" method="post">
                {{ csrf_field() }}
                {{ Form::hidden('sender_id', Auth::id()) }}
                {{ Form::hidden('product_id', $product->id) }}
                <textarea name="comment" class="form-control" rows="3" placeholder="Isi pertanyaan disini"></textarea>
                <input type="submit" class="btn btn-info" style="width: 745px;" value="diskusikan" ></input>
            </form>
            <br>
          </div>
        </div>
        </section>

        <section>
        <div class="row">
        <div class="col-lg-6 col-sm-offset-2">
        <h2>
            Comments
        </h2>

        <table class="table">
        <thead>
            <th>
                Dari
            </th>
            <th>
                Komentar
            </th>
            <th>
                Waktu Komentar
            </th>
        </thead>
        <tbody>
        @foreach($comments as $comment)
            <tr>
                <td>
                   {{ $comment->name}}
                </td>
                <td>
                   {{ $comment->comment}}
                </td>
                <td>
                   {{ $comment->created_at}}
                </td>
            </tr>
        @endforeach
        </tbody>
        </table>
          </div>
        </div>
        </section>
@endsection
