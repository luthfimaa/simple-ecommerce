@extends('layouts.app')
@section('content')
<h1>Edit Profile</h1>
{{ Form::model($user, ['route' => 'update_profile', 'files' => true]) }}
<table>
    <tbody>
        <tr>
            <td>
                Nama
            </td>
            <td>
                {{ Form::text('name') }}
            </td>
        </tr>
        <tr>
            <td>
                Tanggal Lahir
            </td>
            <td>
                {{ Form::date('birth_date') }}
            </td>
        </tr>
        <tr>
            <td>
                Foto
            </td>
            <td>
                {{ Form::file('photo') }}
            </td>
        </tr>
        <tr>
            <td>
                No HP
            </td>
            <td>
                {{ Form::text('phone_number') }}
            </td>
        </tr>
        <tr>
            <td>
                Email
            </td>
            <td>
                {{ Form::email('email') }}
            </td>
        </tr>
        <tr>
            <td>
                Password
            </td>
            <td>
                {{ Form::password('password') }}
            </td>
        </tr>
    </tbody>
</table>
<br>
<br>
{{ Form::submit() }}
{{ Form::close() }}
@endsection
