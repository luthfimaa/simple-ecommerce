@extends('layouts.product')
@section('content')
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::open(['route' => 'send_message']) }}
                {{ Form::hidden('receiver_id', $receiver_id) }}
                {{ Form::hidden('sender_id', $sender_id) }}

                {{ Form::label('message', 'Message') }}
                <br>
                <textarea class="form-control" name="message"></textarea>
                <br>
                <input type="submit" value="kirim" class="btn primary-btn">
                {{ Form::close() }}
            </div>

        </div>
    </div>
@endsection
