﻿@extends('layouts.app')
@section('content')
        </section>


        <!-- Preloader Start -->
        <div class="preloader">
            <div class="rounder"></div>
        </div>
        <!-- Preloader End -->
        <div id="main">
            <div class="container">
                <div class="row">
                    <!-- Portfolio (Right Sidebar) Start -->
                    <div class="col-md-9">
                        <div class="col-md-12 page-body">
                            <div class="row">

                                <div class="col-md-12 content-page">
                                    <div class="col-md-12 blog-post">


                                        <!-- Intro Start -->
                                        <div class="post-title margin-bottom-30">
                                            <h1><span class="main-color">USER LIST</span></h1>
                                        </div>
                                        <!-- Intro End -->




                                        <!-- Portfolio Start -->
                                        <div>


                                            <!-- Portfolio Detail Start -->
                                            <div class="row portfolio">
                                               

                                                <div class="col-sm-6 custom-pad-2">
                                                    <div >
                                                        <table class="table table-bordered">
                                                           <thead>
                                                            <th>
                                                                id
                                                            </th>
                                                            <th>
                                                                name
                                                            </th>
                                                            <th>
                                                               username
                                                            </th>
                                                            <th>
                                                                email
                                                            </th>
                                                            <th>
                                                                phone number
                                                            </th>
                                                            <th>
                                                                birth date
                                                            </th>
                                                            <th>
                                                            </th>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($users as $user)
                                                        @if(!$user->admin)
                                                            <tr>
                                                                <td>
                                                                    {{ $user->id }}
                                                                </td>
                                                                <td>
                                                                    <a href={{ "profile/$user->username" }}>{{ $user->name }}</a>
                                                                </td>
                                                                <td>
                                                                    {{ $user->username }}
                                                                </td>
                                                                <td>
                                                                    {{ $user->email }}
                                                                </td>
                                                                <td>
                                                                    {{ $user->phone_number }}
                                                                </td>
                                                                <td>
                                                                    {{ $user->birth_date }}
                                                                </td>
                                                                <td>
                                                                    {{ Form::open(['route' => 'delete_user']) }}
                                                                    {{ Form::hidden('id', $user->id) }}
                                                                    {{ Form::submit('delete') }}
                                                                    {{ Form::close() }}
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Portfolio Detail End -->




                                    </div>  

                                    <div class="col-md-12 text-center">
                                        <a href="javascript:void(0)" id="load-more-portfolio" class="load-more-button">Load</a>
                                        <div id="portfolio-end-message"></div>
                                    </div>

                                </div>

                            </div>



                            <!-- Subscribe Form Start -->
                            <div class="col-md-8 col-md-offset-2">


                            </div>
                            <!-- Subscribe Form End -->


                        </div>
@endsection
