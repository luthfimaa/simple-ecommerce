@extends('layouts.app')
@section('content')
        </section>


        <!-- Preloader Start -->
        <div class="preloader">
            <div class="rounder"></div>
        </div>
        <!-- Preloader End -->




        <div id="main">
            <div class="container">
                <div class="row">



                   




                    <!-- Portfolio (Right Sidebar) Start -->
                    <div class="col-md-9">
                        <div class="col-md-12 page-body">
                            <div class="row">


                                


                                <div class="col-md-12 content-page">
                                    <div class="col-md-12 blog-post">


                                        <!-- Intro Start -->
                                        <div class="post-title margin-bottom-30">
                                            <h1><span class="main-color">PRODUCT LIST</span></h1>
                                        </div>
                                        <!-- Intro End -->




                                        <!-- Portfolio Start -->
                                        <div>


                                            <!-- Portfolio Detail Start -->
                                            <div class="row portfolio">
                                               

                                                <div class="col-sm-6 custom-pad-2">
                                                    <div class="table">
                                                        <table class="table table-bordered">
                                                        <thead>
                                                            <th>
                                                                id
                                                            </th>
                                                            <th>
                                                                name
                                                            </th>
                                                            <th>
                                                               price
                                                            </th>
                                                            <th>
                                                                category
                                                            </th>
                                                            <th>
                                                                owner id
                                                            </th>
                                                            <th>
                                                                description
                                                            </th>
                                                            <th>
                                                                verified
                                                            </th>
                                                            <th>
                                                            </th>
                                                            <th>
                                                            </th>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($products as $product)
                                                            <tr>
                                                                <td>
                                                                    {{ $product->id }}
                                                                </td>
                                                                <td>
                                                                    <a href={{ "product/$product->id" }}>{{ $product->name }}</a>
                                                                </td>
                                                                <td>
                                                                    {{ $product->price }}
                                                                </td>
                                                                <td>
                                                                    {{ $product->category }}
                                                                </td>
                                                                <td>
                                                                    {{ $product->owner_id }}
                                                                </td>
                                                                <td>
                                                                    {{ $product->description }}
                                                                </td>
                                                                <td>
                                                                    {{ $product->verified }}
                                                                </td>
                                                                <td>
                                                                    {{ Form::open(['route' => 'delete_product']) }}
                                                                    {{ Form::hidden('id', $product->id) }}
                                                                    {{ Form::submit('delete') }}
                                                                    {{ Form::close() }}
                                                                </td>
                                                                <td>
                                                                    {{ Form::open(['route' => 'verify']) }}
                                                                    {{ Form::hidden('id', $product->id) }}
                                                                    {{ Form::submit('verify') }}
                                                                    {{ Form::close() }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Portfolio Detail End -->




                                    </div>  

                                    <div class="col-md-12 text-center">
                                        <a href="javascript:void(0)" id="load-more-portfolio" class="load-more-button">Load</a>
                                        <div id="portfolio-end-message"></div>
                                    </div>

                                </div>

                            </div>



                            <!-- Subscribe Form Start -->
                            <div class="col-md-8 col-md-offset-2">


                            </div>
                            <!-- Subscribe Form End -->


                        </div>
@endsection



        
