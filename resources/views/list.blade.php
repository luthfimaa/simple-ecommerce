@extends('layouts.product')
@section('content')

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h3>Hasil Pencarian "{{ $name }}"</h3>
          </div>
        </div>

        @foreach($products->chunk(3) as $chunk)
        <div class="row">
            @foreach($chunk as $product)
            <div class="col-sm-4 ">
              <div class="wrapper3">
                  <a href="/product/{{ $product->id }}/detail" class="thumbnail">
                    <img src={{ asset('storage\photos'). "/$product->photo" }}></img>
                    <div class="overlay3">
                      <div class="content3">
                        <span>
                            {{ $product->name }}
                        </span>
                        <span>
                            {{ $product->description }}
                        </span>
                      </div>
                    </div>
                    </a>
                </div>
                <p>harga : {{ $product->price }} </p>
                <a href="/product/{{ $product->id }}/detail">
                    <button class="btn btn-primary btn-block btn-sm btn-buy" type="submit">Beli</button>
                </a>
            </div>
            @endforeach
        </div>
        @endforeach
    </section>
    <!-- akhir menu -->

@endsection
