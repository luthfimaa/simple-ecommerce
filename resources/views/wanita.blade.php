@extends('layouts.app')
@section('content')
    <body>

        <!-- jumbotron -->
        <div class="jumbotron text-center">
            <img src="img/SHOP WANITA.png">
        </div>
        <!-- jumbotron -->

        <!-- menu -->
        <section>
            <div class="container">
                <div class="byside">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="text-center">HIGHLIGHT</h3> <hr>
                            <div class="hitam text-center">
                                <img src="img/modelWanita/a.jpg" class="satu">
                                <img src="img/modelWanita/b.jpg" class="dua">
                                <img src="img/modelWanita/c.jpg" class="tiga">
                                <img src="img/modelWanita/d.jpg" class="pat">
                                <img src="img/modelWanita/e.jpg" class="ma">
                                <img src="img/modelWanita/f.jpg" class="nam">
                            </div>
                        </div>


                    </div>
                    </section>

                    <!-- menu 2 -->
                    <section>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3 class="text-center">SHOP BY CATEGORY</h3> <hr>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4 ">
                                    <div class="wrapper3">
                                        <a href="kaos.html" class="thumbnail">
                                            <img src="img/jual/2.jpg">
                                            <div class="overlay3">
                                                <div class="content3">
                                                    <span>kaos</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 ">
                                        <div class="wrapper3">
                                            <a href="" class="thumbnail">
                                                <img src="img/jual/3a.jpg">
                                                <div class="overlay3">
                                                    <div class="content3">
                                                        <span>sandal</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4 ">
                                            <div class="wrapper3">
                                                <a href="sepatu.html" class="thumbnail">
                                                    <img src="img/jual/4.jpg">
                                                    <div class="overlay3">
                                                        <div class="content3">
                                                            <span>sepatu</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    </section>

                                    <!-- MENU DISKON -->
                                    <section>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="text-center">SPESIAL OFFER</h3> <hr>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4 ">
                                                    <div class="wrapper3">
                                                        <a href="" class="thumbnail">
                                                            <img src="img/panjang.jpg">
                                                            <div class="overlay3">
                                                                <div class="content3">
                                                                    <span class="text-center">under 200k</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-2 ">
                                                        <div class="wrapper3">
                                                            <a href="" class="thumbnail">
                                                                <img src="img/freeongkir.jpg">
                                                                <div class="overlay3">
                                                                    <div class="content3">
                                                                        <span class="text-center">free ongkir</span>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-2 ">
                                                            <div class="wrapper3">
                                                                <a href="" class="thumbnail">
                                                                    <img src="img/bayardi.jpg">
                                                                    <div class="overlay3">
                                                                        <div class="content3">
                                                                            <span class="text-center">bayar di tempat</span>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="wrapper3">
                                                                    <a href="" class="thumbnail">
                                                                        <img src="img/palingsering.jpg">
                                                                        <div class="overlay3">
                                                                            <div class="content3">
                                                                                <span class="text-center">paling sering di lihat</span>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        </section>

                                                        <section>
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-sm-5 col-sm-offset-1">
                                                                    <h3>Fashion</h3>
                                                                    <p>Mode atau fesyen (Inggris: fashion) adalah gaya berpakaian yang populer dalam suatu budaya. Secara umum, fesyen termasuk masakan, bahasa, seni, dan arsitektur.Dikarenakan fesyen belum terdaftar dalam bahasa Indonesia, maka mode adalah kata untuk bahasa resminya. Secara etimologi menurut Kamus Besar Bahasa Indonesia, mode merupakan bentuk nomina yang bermakna ragam cara atau bentuk terbaru pada suatu waktu tertentu (tata pakaian, potongan rambut, corak hiasan, dan sebagainya). </p>
                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <h3 class="text-right">Gaya</h3>
                                                                    <p>Gaya dapat berubah dengan cepat. Mode yang dikenakan oleh seseorang mampu mecerminkan siapa si pengguna tersebut.Thomas Carlyle mengatakan, "Pakaian adalah perlambang jiwa. Pakaian tak bisa dipisahkan dari perkembangan sejarah kehidupan dan budaya manusia." Fesyen dimetaforakan sebagai kulit sosial yang membawa pesan dan gaya hidup suatu komunitas tertentu yang merupakan bagian dari kehidupan sosial. Di samping itu, mode juga mengekspresikan identitas tertentu.</p>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <!-- akhir menu -->
@endsection
