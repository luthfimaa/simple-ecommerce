@extends('layouts.app')
@section('content')
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="text-center">Diskusi Produk</h3> <hr>
          </div>
        </div>
        </div>

        <div class="row">
      <div class="col-sm-2 col-sm-offset-2">
      <a href="" class="thumbnail">
        <img src='{{ asset("/storage/photos/{$product->photo}") }}'>
        </a>
          </div>

        <div class="col-sm-5">
          <a href="/product/{{ $product->id }}/detail">
          <button type="button" class="btn btn-info">Informasi produk</button>
          </a>
          <a href="/product/{{ $product->id }}/diskusi">
          <button type="button" class="btn btn-info">Tanya penjual</button><br> <br>
          </a>
          
        <div class="col-sm-5 col-sm-offset-1 text-center">
          <h3 class="te">Informasi penjual</h3>
            <img src="assets/img/avatar_2x.png" class="profile-img-card" style="border-radius: 50%;">
            <form action="/order" method="post" class="form-signin"><span class="reauth-email"> </span>
            {{ csrf_field() }}
            <p>Nama : {{ $owner_name }}
            <button class="btn btn-primary btn-block btn-sm btn-signin" type="submit">Beli</button>
            {{ Form::hidden('product_id', $product->id) }}
            {{ Form::hidden('buyer_id', $user_id) }}
        </form>
        </div>
          </div>
        </div>



        <section>
        <div class="row">
        <div class="col-lg-6 col-sm-offset-2">
        <a name="chat" ></a>
            <h4 class="">Ajukan pertanyaan mengenai produk ini!</h4>
          </div>
        </div>
        </section>



        <div class="row">
          <div class="col-sm-6 col-sm-offset-2">
        
        <div class="chatbox">
        <div class="chatlogs">

        @foreach($messages as $message)
          @if(Auth::user()->id == $message->sender)
          <div class="chat pelanggan">
          <div class="photo"><img src='{{ asset("/storage/photos/{$user->photo}") }}'></div>
            <p class="pesan">{{ $message->message}}</p>
          </div>
          @else
          <div class="chat owner">
          <div class="photo"><img src='{{ asset("/storage/photos/{$user->photo}") }}'></div>
            <p class="pesan">{{ $message->message}}</p>
          </div>
          @endif
        @endforeach

      </div>
      <div class="form">
        <textarea></textarea>
        <button>Kirim</button>
      </div>
      </div>

      </div>
      </div>
@endsection