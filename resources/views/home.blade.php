@extends('layouts.home')
  <!-- akhir navbar -->

@section('content')
  <!-- slide gambar -->
<section class="slider">
        <div class="slider__half slider__half--top" >
            <div class="slider__slide-channel">

                <div class="slider__top-slide-item">
                    <img src="img/slider/ho16-mens-jackets-banner.jpg">
                </div>

                <div class="slider__top-slide-item" >
                    <img src="img/slider/sp17-mens-theboys-shorts-category.jpg">
                </div>

                <div class="slider__top-slide-item" >
                    <img src="img/slider/sp17-mens-mason_hat-cat-banner.jpg">
                </div>

                <div class="slider__top-slide-item" >
                    <img src="img/slider/ho16-mens-jackets-banner.jpg">
                </div>

            </div>
            <div class="slider__text slider__text--top">
                <div class="slider__text__content"></div>
            </div>
        </div>
        
        <div class="slider__half slider__half__bottom" >
            <div class="slider__slide-channel">

                <div class="slider__bottom-slide-item" >
                    <img src="img/slider/su17-mens_tees-category.jpg">
                </div>

                <div class="slider__bottom-slide-item" >
                    <img src="img/slider/fa17-tom_curren-footwear-category.jpg">
                </div>

                <div class="slider__bottom-slide-item" >
                    <img src="img/slider/sp17-mens-new-arrivals-mason_ho-category.jpg">
                </div>

                <div class="slider__bottom-slide-item" >
                    <img src="img/slider/su17-mens_tees-category.jpg">
                </div>

            </div>

            <div class="slider__text slider__text--bottom">
                <div class="slider__text__content"></div>
            </div>
        </div>
    </section>
 <!-- akhir slide gambar -->


  <!-- jumbotron -->
    <div class="jumbotron text-center">
      <img src="img/logo.png">
      <p>A store maker inspired from the youth culture and every raddest thing that you can imagine. Based in Malang Indonesia since 2017</p>
    </div>
    <!-- jumbotron -->

<!-- menu -->
    <section>
      <div class="container">
      <div class="byside">
        <div class="row">
          <div class="col-sm-12">
            <h3 class="text-center">CHOOSE ONE</h3> <hr>
          </div>

      <div class="row">
      <div class="col-sm-4 col-sm-offset-2 ">
      <div class="wrapper1">
      <a href="/pria" class="thumbnail">
        <img src="img/men.jpg">
        <div class="overlay1">
          <div class="content1">
            <span class="text-center">men to the left</span>
          </div>
        </div>
        </a>
        </div>
      </div>

      <div class="row">
      <div class="col-sm-4 ">
      <div class="wrapper2">
      <a href="/wanita" class="thumbnail">
        <img src="img/ladies.jpg">
        <div class="overlay2">
          <div class="content2">
            <span class="text-center">women are always right</span>
          </div>
        </div>
        </a>
        </div>
      </div>

        </div>
        </div>
      </div>
    </section>

<!-- menu 2 -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h3 class="text-center">FEATURED BRAND</h3> <hr>
          </div>
        </div>

      <div class="row">
      <div class="col-sm-4 ">
      <div class="wrapper3">
      <a href="" class="thumbnail">
        <img src="img/brand/2001_322x165_surfingbrandsRev.jpg">
        <div class="overlay3">
          <div class="content3">
            <span>surf</span>
          </div>
        </div>
        </a>
        </div>
      </div>

      <div class="row">
      <div class="col-sm-4 ">
      <div class="wrapper3">
      <a href="" class="thumbnail">
        <img src="img/brand/2004_322x165_adidaszerodye_men1.jpg">
        <div class="overlay3">
          <div class="content3">
            <span>adidas</span>
          </div>
        </div>
        </a>
        </div>
      </div>

      <div class="row">
      <div class="col-sm-4 ">
      <div class="wrapper3">
      <a href="" class="thumbnail">
        <img src="img/brand/nike_322x165_feb07.jpg">
        <div class="overlay3">
          <div class="content3">
            <span>nike</span>
          </div>
        </div>
        </a>
        </div>
      </div>
      </div>
      </section>

      <!-- MENU DISKON -->
      <section></section>
      <section>
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <h3 class="text-center">SPESIAL OFFER</h3>
              <hr class="te text-center" style="width: 2px;"> 
              <hr class="garis">
            </div>
          </div>
          </div>
      <section>
        <ul id="hexGrid">
      <li class="hex">
        <div class="hexIn">
          <a class="hexLink" href="#">
            <img src="img/gambar hexa/1.png">
            <h1>DIBAWAH 200K</h1>
            <p>Barang dengan harga di bawah 200.000 ribu</p>
          </a>
        </div>
      </li>

      <li class="hex">
        <div class="hexIn">
          <a class="hexLink" href="#">
            <img src="img/gambar hexa/2.png">
            <h1>BAYAR DI TEMPAT</h1>
            <p>Lakukan pembayaran setelah sampai tujuan</p>
          </a>
        </div>
      </li>
     
      <li class="hex">
        <div class="hexIn">
          <a class="hexLink" href="#">
            <img src="img/gambar hexa/10.png">
            <h1>BARANG BARU</h1>
            <p>Produk yang baru saja di datang</p>
          </a>
        </div>
      </li>

      <li class="hex">
        <div class="hexIn">
          <a class="hexLink" href="#">
            <img src="img/gambar hexa/4.png">
            <h1>PALING SERING DI LIHAT</h1>
            <p>Produk yang sering di lihat oleh customer</p>
          </a>
        </div>
      </li>

      <li class="hex">
        <div class="hexIn">
          <a class="hexLink" href="#">
            <img src="img/gambar hexa/7.png">
            <h1>FREE ONGKIR</h1>
            <p>Gratis ongkos pengiriman barang</p>
          </a>
        </div>
        </li>
      </section>

      <section>
      <hr>
      <div class="row">
        <div class="col-sm-5 col-sm-offset-1">
        <h3>Fashion</h3>
        <p>Mode atau fesyen (Inggris: fashion) adalah gaya berpakaian yang populer dalam suatu budaya. Secara umum, fesyen termasuk masakan, bahasa, seni, dan arsitektur.Dikarenakan fesyen belum terdaftar dalam bahasa Indonesia, maka mode adalah kata untuk bahasa resminya. Secara etimologi menurut Kamus Besar Bahasa Indonesia, mode merupakan bentuk nomina yang bermakna ragam cara atau bentuk terbaru pada suatu waktu tertentu (tata pakaian, potongan rambut, corak hiasan, dan sebagainya). </p>
        </div>
<div class="col-sm-5">
  <h3 class="text-right">Gaya</h3>
        <p>Gaya dapat berubah dengan cepat. Mode yang dikenakan oleh seseorang mampu mecerminkan siapa si pengguna tersebut.Thomas Carlyle mengatakan, "Pakaian adalah perlambang jiwa. Pakaian tak bisa dipisahkan dari perkembangan sejarah kehidupan dan budaya manusia." Fesyen dimetaforakan sebagai kulit sosial yang membawa pesan dan gaya hidup suatu komunitas tertentu yang merupakan bagian dari kehidupan sosial. Di samping itu, mode juga mengekspresikan identitas tertentu.</p>
    </div>
  </div>
    </section>
    <!-- akhir menu -->

@endsection
