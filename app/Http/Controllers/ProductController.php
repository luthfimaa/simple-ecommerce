<?php

namespace App\Http\Controllers;

use App\Product;
use App\Comment;
use App\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    function detail($id){
        $product = Product::find($id);
        $user_id = Auth::user()->id;
        $comments = $product->comments()
                    ->join('users', 'users.id', '=', 'sender_id')
                    ->get(['users.name', 'comments.comment', 'comments.created_at']);

        $data = ['product' => $product, 'owner' => $product->owner, 'user_id' => $user_id, 'comments' => $comments];

        return view('detail', $data);
    }

    function diskusi($id){
        $product = Product::find($id);
        $user_id = Auth::user()->id;
        $comments = $product->comments()
                    ->join('users', 'users.id', '=', 'sender_id')
                    ->orderBy('created_at')
                    ->get(['comments.id', 'users.name', 'comments.comment', 'comments.created_at']);

        $data = ['product' => $product, 'owner' => $product->owner, 'user_id' => $user_id, 'comments' => $comments];

        return view('diskusi', $data);
    }

    function show(){
        return view('create_form');
    }

    function create(Request $request){
        $product = new Product;
        $full_path = $request->file('photo')->store('public/photos');
        $path = explode('/', $full_path)[2];

        $product->fill($request->all());
        $product->photo = $path;
        $product->save();

        return redirect('/');
    }

    function show_products(){
        $products = Auth::user()->products;
        return view('products', compact('products'));
    }

    function edit_product(Request $request){
        $product = Product::find($request->id);
        return view('edit_product', compact('product'));
    }

    function update_product(Request $request){
        $product = Product::find($request->id);

        if($request->photo == null){
            $product->fill($request->except('photo'));
        }
        else{
            $product->fill($request->all());
        }

        $product->sold = $request->sold;
        $product->commentable = $request->commentable;
        $product->update();
        return redirect('/profile');
    }

    function search(Request $request){
        $name = $request->all()['query'];
        $products = Product::where('name', 'LIKE', "%$name%")->get();
        $data = ['name' => $name, 'products' => $products];
        return view('list', $data);
    }
}
