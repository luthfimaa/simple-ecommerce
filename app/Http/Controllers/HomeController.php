<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
    public function show_by_category($category){
        $latest_products = Product::where('category', '=', $category)
                    ->orderBy('created_at'); 

        $products = $latest_products
                    ->join('users', 'users.id', '=', 'products.owner_id')
                    ->select(['products.*', 'users.name as owner_name'])
                    ->get();
        //return view($category);
        $products = $products->filter(function($value, $key){
            return $value->verified == 1;
        });
        return view('category', ['products' => $products, 'category' => $category] );
    }

    public function show_by_gender($gender){
        $latest_products = Product::where('gender', '=', $gender)
                    ->orderBy('created_at'); 

        $products = $latest_products
                    ->join('users', 'users.id', '=', 'products.owner_id')
                    ->select(['products.*', 'users.name as owner_name'])
                    ->get();
        return view($gender);
        //return view('category', ['products' => $products, 'category' => $category] );
    }

    public function admin()
    {
        return view('admin');
    }

    public function pria(){
        return view('pria');
    }

    public function wanita(){
        return view('wanita');
    }
}
