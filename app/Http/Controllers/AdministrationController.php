<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;

class AdministrationController extends Controller
{
    function show_users(){
        $users = User::get();
        return view('user_list', compact('users'));
    }

    function show_products(){
        $products = Product::get();
        return view('product_list', compact('products'));
    }

    function delete_user(Request $request){
        //User::find($request->id)->delete();
        $user = User::find($request->id);
        foreach($user->products() as $product){
            $product->delete();
        }
        $user->delete();
        return redirect('/user_list');
    }

    function delete_product(Request $request){
        Product::find($request->id)->delete();
        return redirect('/product_list');
    }
    function verify_product(Request $request){
        $product = Product::find($request->id);
        $product->verified = 1;
        $product->update();
        return redirect('/product_list');
    }

}
