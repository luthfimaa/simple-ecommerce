<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Product;

class ProfileController extends Controller
{
    function show_profile($username){
        $user = User::where('username', '=', $username)->first();
        return view('profile', compact('user'));
    }

    function show_self(){
        $user = Auth::user();
        $messages = $user->messages()->join('users', 'users.id', '=', 'sender_id')->get(['users.name', 'messages.message']);
        $products = $user->products()->get();
        $data = ['user' => $user, 'messages' => $messages, 'products' => $products];
        return view('self', $data);
    }
    function edit_form(){
        $user = Auth::user();
        return view('edit_profile', compact('user'));
    }

    function update_profile(Request $request){
        $user = Auth::user();
        $user->fill($request->except('photo'));

        if($request->photo != null){
            $full_path = $request->file('photo')->store('public/photos');
            $path = explode('/', $full_path)[2];
            $user->photo = $path;
        }

        $user->password = bcrypt($request->password);
        $user->phone_number = $request->phone_number;
        $user->update();
        return redirect('/profile');

    }
}
