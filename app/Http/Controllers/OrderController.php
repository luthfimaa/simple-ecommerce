<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\User;
use App\Message;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    function order(Request $request){
        $order = new Order();
        $order->create($request->all());

        $product = Product::find($request->product_id);
        $buyer = User::find($request->buyer_id);

        $message = new Message();
        $message->sender_id = $buyer->id;
        $message->receiver_id = $product->owner->id;
        $message->message = "membeli $product->name";

        echo $message->message;
        $message->save();

        return redirect('/');
    }
}
