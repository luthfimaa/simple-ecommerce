<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Message;
use App\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    function send(Request $request){
        $comment = new Comment();
        $comment->create($request->all());

        $product = Product::find($request->product_id);
        $owner = $product->belongsTo('App\User', 'owner_id')->first();

        $message = new Message();
        $message->sender_id = Auth::user()->id;
        $message->receiver_id = $owner->id;
        $message->message = Auth::user()->name . " mengomentari iklan " . $product->name;
        $message->save();

        return redirect('/product/' . $request->product_id . '/detail');
    }
}
