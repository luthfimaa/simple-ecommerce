<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Message;

class MessageController extends Controller
{
    function send(Request $request){
        $message = new Message();
        $message->create($request->all());
        return redirect('/');
    }

    function show(Request $request){
        $receiver_id = $request->id;
        $sender_id = Auth::user()->id;
        $data = ['sender_id' => $sender_id, 'receiver_id' => $receiver_id];
        return view('message_box', $data); 
    }
}
