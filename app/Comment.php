<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'sender_id', 'product_id', 'comment',
    ];
    
}
