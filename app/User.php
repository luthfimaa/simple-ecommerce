<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Message;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'photo' => 'profile.jpg', 'phone_number' => 0, 'birth_date' => '1990-01-01', 'admin' => 0,
    ];
    protected $fillable = [
        'name', 'email', 'password', 'username', 'birth_date', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function messages(){
        return $this->hasMany('App\Message', 'receiver_id');
    }

    function products(){
        return $this->hasMany('App\Product', 'owner_id');
    }
}
