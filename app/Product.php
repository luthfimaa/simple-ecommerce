<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $attributes = [
        'photo' => 'photo.jpg', 'sold' => 0, 'commentable' => 1, 'verified' => 0,
    ];

    protected $fillable = [
        'name', 'price', 'category', 'photo', 'owner_id', 'description', 'gender'
    ];
    //
    function comments(){
        return $this->hasMany('App\Comment');
    }

    function owner(){
        return $this->belongsTo('App\User');
    }
}
